package facci.angelinepico.sugarorm;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText editTitulo, editEdicion, editID;
    Button buttonGuardar, buttonModificar, buttonConsultaIndividual,
            buttonGeneral, buttonEliminar, buttonEliminartodo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTitulo=findViewById(R.id.editTitulo);
        editEdicion=findViewById(R.id.editEdicion);
        editID=findViewById(R.id.editID);
        //CREATE
        buttonGuardar=findViewById(R.id.buttonGuardar);
        //READ
        buttonGeneral=findViewById(R.id.buttonGeneral);
        buttonConsultaIndividual=findViewById(R.id.buttonConsultaIndividual);
        //UPADATE
        buttonModificar=findViewById(R.id.buttonModificar);
        //DELETE
        buttonEliminar=findViewById(R.id.buttonEliminar);
        buttonEliminartodo=findViewById(R.id.buttonEliminartodo);

        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book registro1=new Book(
                        editTitulo.getText().toString(),
                        editEdicion.getText().toString());
                registro1.save();
                Log.e("Guardar", "Datos guardados");

            }
        });
        buttonConsultaIndividual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book= Book.findById(
                        Book.class,
                        Long.parseLong(editID.getText().toString())
                );
                editTitulo.setText(book.getTitle());
                editEdicion.setText(book.getEdition());
            }
        });


        buttonModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book=Book.findById(Book.class,Long.parseLong("1"));
                book.title="update title here";//edittext titulo
                book.edition="3ed edition";//edittext edicion
                book.save();

            }
        });

    }
}
