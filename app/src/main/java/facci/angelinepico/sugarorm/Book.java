package facci.angelinepico.sugarorm;


import com.orm.SugarRecord;

import java.security.PublicKey;

public class Book extends SugarRecord <Book>{
    String title;
    String edition;

    public Book(String title, String edition) {
        this.title = title;
        this.edition = edition;
    }
    public Book(){

    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }
}

